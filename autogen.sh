#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gnome-mag"

USE_GNOME2_MACROS=1 REQUIRED_AUTOMAKE_VERSION=1.9 . gnome-autogen.sh
