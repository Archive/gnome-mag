/*
 * AT-SPI - Assistive Technology Service Provider Interface
 * (Gnome Accessibility Project; http://developer.gnome.org/projects/gap)
 *
 * Copyright 2001 Sun Microsystems Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef ZOOM_REGION_H_
#define ZOOM_REGION_H_


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <bonobo/bonobo-object.h>
#include "GNOME_Magnifier.h"

#define ZOOM_REGION_TYPE         (zoom_region_get_type ())
#define ZOOM_REGION(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), ZOOM_REGION_TYPE, ZoomRegion))
#define ZOOM_REGION_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), ZOOM_REGION_TYPE, ZoomRegionClass))
#define IS_ZOOM_REGION(o)        (G_TYPE_CHECK__INSTANCE_TYPE ((o), ZOOM_REGION_TYPE))
#define IS_ZOOM_REGION_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), ZOOM_REGION_TYPE))
#define ZOOM_REGION_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), ZOOM_REGION_TYPE, ZoomRegionClass))

typedef GList * (*CoalesceFunc)(GList *, int);

typedef struct _ZoomRegionPrivate ZoomRegionPrivate;

typedef struct {
        BonoboObject parent;
	BonoboPropertyBag *properties;
	gboolean invert;
	gboolean is_managed;
	gboolean draw_cursor;
	gboolean cache_source;
	gchar *smoothing;
	gchar *object_path;
	gfloat contrast;
	gdouble xscale;
	gdouble yscale;
	gdouble contrast_r;
	gdouble contrast_g;
	gdouble contrast_b;
	gdouble bright_r;
	gdouble bright_g;
	gdouble bright_b;
	gint border_size_top;
	gint border_size_left;
	gint border_size_right;
	gint border_size_bottom;
	guint32 border_color; /* A-RGB, 8 bits each, MSB==alpha */
	gint x_align_policy;  /* TODO: enums here */
	gint y_align_policy;
	GNOME_Magnifier_ZoomRegion_ScrollingPolicy smooth_scroll_policy;
	GNOME_Magnifier_ZoomRegion_ColorBlindFilter color_blind_filter;
        /* bounds of viewport, in target magnifier window coords */
	GNOME_Magnifier_RectBounds roi;
	GNOME_Magnifier_RectBounds viewport; 
	ZoomRegionPrivate *priv;
	CoalesceFunc coalesce_func;
	gint timing_iterations;
	gboolean timing_output;
	gint timing_pan_rate;
	gboolean exit_magnifier;
        gboolean poll_mouse;
#ifdef ZOOM_REGION_DEBUG
        gboolean alive;
#endif
} ZoomRegion;

typedef struct {
        BonoboObjectClass                    parent_class;
        POA_GNOME_Magnifier_ZoomRegion__epv  epv;
        DBusGConnection                     *connection;
} ZoomRegionClass;

GType     zoom_region_get_type (void);
ZoomRegion *zoom_region_new     (void);

/* D-BUS methods */
gboolean	impl_dbus_zoom_region_set_mag_factor (ZoomRegion *zoom_region, const gdouble mag_factor_x,
			const gdouble mag_factor_y);
GArray*		impl_dbus_zoom_region_get_mag_factor (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_roi (ZoomRegion *zoom_region, const gint32 **roi);
gboolean	impl_dbus_zoom_region_update_pointer (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_mark_dirty (ZoomRegion *zoom_region, gint32 **bounds);
GArray*		impl_dbus_zoom_region_get_roi (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_move_resize (ZoomRegion *zoom_region, const gint32 **viewport);
gboolean	impl_dbus_zoom_region_dispose (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_pointer_pos (ZoomRegion *zoom_region, gint32 mouse_x, gint32 mouse_y);
gboolean	impl_dbus_zoom_region_set_contrast (ZoomRegion *zoom_region, gdouble R, gdouble G, gdouble B);
GArray*		impl_dbus_zoom_region_get_contrast (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_brightness (ZoomRegion *zoom_region, gdouble R, gdouble G, gdouble B);
GArray*		impl_dbus_zoom_region_get_brightness (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_managed (ZoomRegion *zoom_region, gboolean managed);
gboolean	impl_dbus_zoom_region_get_managed (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_poll_mouse (ZoomRegion *zoom_region, gboolean poll_mouse);
gboolean	impl_dbus_zoom_region_get_poll_mouse (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_draw_cursor (ZoomRegion *zoom_region, gboolean draw_cursor);
gboolean	impl_dbus_zoom_region_get_draw_cursor (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_invert (ZoomRegion *zoom_region, gboolean invert);
gboolean	impl_dbus_zoom_region_get_invert (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_smoothscroll (ZoomRegion *zoom_region, gshort smoothscroll);
gshort		impl_dbus_zoom_region_get_smoothscroll (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_colorblind (ZoomRegion *zoom_region, gshort colorblind);
gshort		impl_dbus_zoom_region_get_colorblind (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_smoothing (ZoomRegion *zoom_region, gchar *smoothing);
gchar*		impl_dbus_zoom_region_get_smoothing (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_testpattern (ZoomRegion *zoom_region, gboolean test);
gboolean	impl_dbus_zoom_region_get_testpattern (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_bordersizes (ZoomRegion *zoom_region, gint32 **bordersizes);
GArray*		impl_dbus_zoom_region_get_bordersizes (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_bordercolor (ZoomRegion *zoom_region, guint32 bordercolor);
guint32		impl_dbus_zoom_region_get_bordercolor (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_xalign (ZoomRegion *zoom_region, gint32 align);
gint32		impl_dbus_zoom_region_get_xalign (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_yalign (ZoomRegion *zoom_region, gint32 align);
gint32		impl_dbus_zoom_region_get_yalign (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_viewport (ZoomRegion *zoom_region, gint32 **viewport);
GArray*		impl_dbus_zoom_region_get_viewport (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_timing_test (ZoomRegion *zoom_region, gint32 timing_iterations);
gint32		impl_dbus_zoom_region_get_timing_test (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_timing_output (ZoomRegion *zoom_region, gboolean timing_output);
gboolean	impl_dbus_zoom_region_get_timing_output (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_timing_pan_rate (ZoomRegion *zoom_region, gint32 timing_pan_rate);
gint32		impl_dbus_zoom_region_get_timing_pan_rate (ZoomRegion *zoom_region);
gboolean	impl_dbus_zoom_region_set_exit_magnifier (ZoomRegion *zoom_region, gboolean exit_magnifier);
gboolean	impl_dbus_zoom_region_get_exit_magnifier (ZoomRegion *zoom_region);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* ZOOM_REGION_H_ */
